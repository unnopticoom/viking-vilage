﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wind_FMOD : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string windEvent;

    FMOD.Studio.EventInstance windInstance;


    // Start is called before the first frame update
    void Start()
    {
        //FMODUnity.RuntimeManager.PlayOneShot(windEvent);
        windInstance = FMODUnity.RuntimeManager.CreateInstance(windEvent);
        windInstance.start();
    }

    // Update is called once per frame
    void Update()
    {
        //windInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
